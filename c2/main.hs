-- 3
n = a `div` length xs
  where
    a  = 10
    xs = [1,2,3,4,5]

  -- Name of the function in lower case
  -- ` instead of '
  -- spacing of xs?

-- 4 
myLast :: [a] -> a
myLast = head . reverse

-- 5
myInit :: [a] -> [a]
myInit l = take (length l - 1) l

myOtherInit :: [a] -> [a]
myOtherInit = reverse . tail .reverse

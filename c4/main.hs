test :: [Char] -> Bool
test ('a' : _) = True
test _         = False

-- 1  

halve :: [a] -> ([a], [a])
halve x = (take s x, drop s x) where s = div (length x) 2

-- 2

thirda :: [a] -> a
thirda = head . tail . tail

thirdb :: [a] -> a
thirdb x = x !! 2

thirdc :: [a] -> a
thirdc (_ : _ : x : xs) = x


-- 3

safetaila :: [a] -> [a]
safetaila xs = if null xs then [] else tail xs

safetailb :: [a] -> [a]
safetailb xs | null xs   = []
             | otherwise = tail xs

safetailc :: [a] -> [a]
safetailc []       = []
safetailc (x : xs) = xs

-- 4

 -- except exhaustive

myora :: Bool -> Bool -> Bool
myora True _    = True
myora _    True = True


myorb :: Bool -> Bool -> Bool
myorb False False = False
myorb _     _     = True


myorc :: Bool -> Bool -> Bool
myorc False b = b
myorc True  b = True

myord :: Bool -> Bool -> Bool
myord b c | b == c    = b
          | otherwise = True

-- 5 

myand :: Bool -> Bool -> Bool
myand a b = if a then if b then True else False else False

-- 6

myotherand :: Bool -> Bool -> Bool
myotherand a b = if a then b else False

-- 7

mult :: Int -> Int -> Int -> Int
--mult x y z = x*y*z
mult = \x -> (\y -> (\z -> x * y * z))

--8

luhnDouble :: Int -> Int
luhnDouble x = if y > 9 then y - 9 else y where y = 2 * x

luhn :: Int -> Int -> Int -> Int -> Bool
luhn a b c d = mod l 10 == 0 where l = sum [luhnDouble a, b, luhnDouble c, d]

factors :: Int -> [Int]
factors n = [ x | x <- [1 .. (div n 2)], mod n x == 0 ] ++ [n]

prime :: Int -> Bool
prime n = factors n == [1, n]

primes :: Int -> [Int]
primes n = [ x | x <- [2 .. n], prime x ]

--Cipher on cipher.hs

-- 1

sumSquares = sum [ x ^ 2 | x <- [1 .. 100] ]

-- 2
grid :: Int -> Int -> [(Int, Int)]
grid m n = [ (x, y) | x <- [0 .. m], y <- [0 .. n] ]

-- 3
square :: Int -> [(Int, Int)]
square n = [ (x, y) | (x, y) <- grid n n, x /= y ]

-- 4 
myReplicate :: Int -> a -> [a]
myReplicate n a = [ a | _ <- [1 .. n] ]

-- 5
pyths :: Int -> [(Int, Int, Int)]
pyths n =
  [ (x, y, z)
  | x <- [1 .. n]
  , y <- [1 .. n]
  , z <- [1 .. n]
  , x ^ 2 + y ^ 2 == z ^ 2
  ]

-- 6
perfects :: Int -> [Int]
perfects n = [ x | x <- [1 .. n], sum (factors x) - x == x ]

-- 7
l1 = [ (x, y) | x <- [1, 2], y <- [3, 4] ]
l2 = concat [ [ (x, y) | y <- [3, 4] ] | x <- [1, 2] ]

-- 8
  -- from cipher.hs
positions :: Eq a => a -> [a] -> [Int]
positions x xs = [ i | (x', i) <- zip xs [0 ..], x == x' ]
  -- from p. 56
myFind :: Eq a => a -> [(a, b)] -> [b]
myFind k t = [ v | (k', v) <- t, k == k' ]

myPositions :: Eq a => a -> [a] -> [Int]
myPositions x xs = myFind x $ zip xs [0 ..]

-- 9
scalarproduct :: [Int] -> [Int] -> Int
scalarproduct xs ys = sum [ x * y | (x, y) <- zip xs ys ]

--10
-- TODO: adapt cipher to work on upper case p.62 libro

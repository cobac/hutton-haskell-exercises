------- String transmission

import           Data.Char
import           Data.List --for sort

type Bit = Int

bin2int :: [Bit] -> Int
--bin2int bits = sum [w*b | (w,b) <- zip weights bits]
--  where weights = iterate (*2) 1
bin2int = foldr (\x y -> x + 2 * y) 0

int2bin :: Int -> [Bit]
int2bin 0 = []
int2bin n = mod n 2 : int2bin (div n 2)

make8 :: [Bit] -> [Bit]
make8 bits = take 8 (bits ++ repeat 0)

encode :: String -> [Bit]
encode = concat . map (make8 . int2bin . ord)

chop8 :: [Bit] -> [[Bit]]
chop8 []   = []
chop8 bits = take 8 bits : chop8 (drop 8 bits)

decode :: [Bit] -> String
decode = map (chr . bin2int) . chop8

transmit :: String -> String
transmit = decode . channel . encode

channel :: [Bit] -> [Bit]
channel = id

----------- Voting algorithms
  -- simple
votes :: [String]
votes = ["Red", "Blue", "Green", "Blue", "Blue", "Red"]

count :: Eq a => a -> [a] -> Int
count x = length . filter (== x)

rmdups :: Eq a => [a] -> [a]
rmdups []       = []
rmdups (x : xs) = x : filter (/= x) (rmdups xs)

result :: Ord a => [a] -> [(Int, a)]
result vs = sort [ (count v vs, v) | v <- rmdups vs ]

winner :: Ord a => [a] -> a
winner = snd . last . result

  -- alternative
ballots :: [[String]]
ballots =
  [ ["Red", "Green"]
  , ["Blue"]
  , []
  , --added by me
    ["Green", "Red", "Blue"]
  , ["Blue", "Green", "Red"]
  , ["Green"]
  ]

rmempty :: Eq a => [[a]] -> [[a]]
rmempty = filter (/= [])

elim :: Eq a => a -> [[a]] -> [[a]]
elim x = map $ filter (/= x)

rank :: Ord a => [[a]] -> [a]
rank = map snd . result . map head

winner' :: Ord a => [[a]] -> a
winner' bs = case rank (rmempty bs) of
  [c     ] -> c
  (c : cs) -> winner' (elim c bs)

-------- Exercises
  -- 1
mapfilter :: (a -> Bool) -> (a -> b) -> [a] -> [b]
mapfilter p f = map f . filter p

  -- 2
  --nein

  -- 3
mymap :: (a -> b) -> [a] -> [b]
mymap f = foldr (\x a -> f x : a) []

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter f = foldr (\x a -> if f x then x : a else a) []

-- 4
dec2int :: [Int] -> Int
dec2int = foldl (\a x -> a * 10 + x) 0

-- 5
-- lambdarolloperomeh

mycurry :: ((a, b) -> c) -> a -> b -> c
mycurry f a b = f (a, b)

myuncurry :: (a -> b -> c) -> (a, b) -> c
myuncurry f (a, b) = f a b

-- 6 7 8
  -- transmitter.hs

-- 9
altmap :: (a -> b) -> (a -> b) -> [a] -> [b]
altmap f g []             = []
altmap f g [x           ] = [f x]
altmap f g (x1 : x2 : xs) = f x1 : g x2 : altmap f g xs

-- 10
luhn :: [Int] -> Bool
luhn l = mod (sum $ altmap id doublemod9 $ reverse l) 10 == 0

doublemod9 :: Int -> Int
doublemod9 x = mod (x * 2) 9

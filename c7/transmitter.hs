import           Data.Char
--import           Data.List --for sort

type Bit = Int

unfold p h t x | p x       = []
               | otherwise = h x : unfold p h t (t x)

-- redefined with unfold
int2bin :: Int -> [Bit]
int2bin = unfold (== 0) (`mod` 2) (`div` 2)

chop9 :: [Bit] -> [[Bit]]
chop9 = unfold null (take 9) (drop 9)
--chop8 []   = []
--chop8 bits = take 8 bits : chop8 (drop 8 bits)

mymap :: (a -> b) -> [a] -> [b]
mymap f = unfold null (f . head) tail

myiterate :: (a -> a) -> a -> [a]
myiterate = unfold (const False) id

-- The transmitter

bin2int :: [Bit] -> Int
bin2int = foldr (\x y -> x + 2 * y) 0

make8 :: [Bit] -> [Bit]
make8 bits = take 8 (bits ++ repeat 0)

bit2byte :: [Bit] -> [Bit]
bit2byte bits = bits ++ parity
 where
  parity | even $ sum bits = [0]
         | otherwise       = [1]

encode :: String -> [Bit]
encode = concat . map (bit2byte . make8 . int2bin . ord)


validatebyte :: [Bit] -> [Bit]
validatebyte bits | parity == evenones = init bits
                  | otherwise          = error "There is a corrupted byte"
 where
  parity   = even $ last bits
  evenones = even $ sum $ init bits

decode :: [Bit] -> String
decode = map (chr . bin2int . validatebyte) . chop9

goodchannel :: [Bit] -> [Bit]
goodchannel = id

badchannel :: [Bit] -> [Bit]
badchannel = tail

transmit :: ([Bit] -> [Bit]) -> String -> String
transmit channel = decode . channel . encode



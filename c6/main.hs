fib :: Int -> Int
fib 0 = 0
fib 1 = 1
fib n = fib (n - 2) + fib (n - 1)

insert :: Ord a => a -> [a] -> [a]
insert x [] = [x]
insert x (y:ys) | x <= y = x : y : ys
                | otherwise = y : insert x ys

isort :: Ord a => [a] -> [a]
isort [] = []
isort (x:xs)= insert x (isort xs)

-- Exercise 1
-- Infinite loop with negative numbers
fac :: Integral a => a -> a
fac 0 = 1
fac n | n < 0 = 1
      | otherwise = n * fac (n-1)

-- Exercise 2
sumdown :: Integral a => a -> a
sumdown 0 = 0
sumdown n = n + sumdown (n-1)

-- Exercise 3
power :: Integral a => a -> a -> a
power 0 _ = 0
power _ 0 = 1
power b p = b * (power b (p-1))

-- Exercise 4
euclid :: Int -> Int -> Int
euclid a b | a==b = a
           | otherwise = euclid small (large - small)
          where 
            small = min a b
            large = max a b 


-- Exercise 5
-- noxd

-- 6
myand :: [Bool] -> Bool
myand [] = True
myand (b:bs) = b && myand bs

myconcat :: [[a]] -> [a]
myconcat [] = []
myconcat (x:xs) = x ++ myconcat xs

myrep :: Int -> a -> [a]
myrep 0 _ = []
myrep n x = x : myrep (n-1) x

select :: [a] -> Int -> a
--select [] _     = []
select (x:xs) 0 = x
select (x:xs) n = select xs (n-1)

myelem :: Eq a => a -> [a] -> Bool
myelem _ [] = False
myelem y (x:xs) | x == y = True
                | otherwise = myelem y xs

-- pfu

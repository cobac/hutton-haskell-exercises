-- 1

l1 = ['a', 'b', 'c'] -- [Char]
l2 = ('a', 'b', 'c') -- (Char, Char, Char)
l3 = [(False, 'O'), (True, '1')] -- [(Bool, Char)]
l4 = ([False, True], ['0', '1']) -- ([Bol], [Char])
l5 = [tail, init, reverse] --- [[a] -> [a]]

-- 2

-- [True, True, False] :: [Bool]

-- [[1,2,3], [1,3,3], [2]] :: [[Int]]

add :: Int -> Int -> Int -> Int
add x y z = x + y + z

copy :: a -> (a, a)
copy a = (a, a)

apply :: (a -> b) -> a -> b
apply f = f

-- 3

second :: [a] -> a
second = head . tail

swap :: (a, b) -> (b, a)
swap (x, y) = (y, x)

pair :: a -> b -> (a, b)
pair x y = (x, y)

double :: Num a => a -> a
double x = x * 2

palindrome :: Eq a => [a] -> Bool
palindrome xs = reverse xs == xs

twice :: (a -> a) -> a -> a
twice f x = f (f x)

-- 4

-- 5
-- Because then they are the same function

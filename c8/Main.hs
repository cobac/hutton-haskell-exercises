-- Just type alias
type Pos = (Int, Int)

-- Parametric type
type Assoc k v = [(k, v)]

find :: Eq k => k -> Assoc k v -> v
find k t = head [ v' | (k', v') <- t, k' == k ]


data Move = North | South | East | West
  deriving (Show, Eq)

move :: Move -> Pos -> Pos
move North (x, y) = (x, y + 1)
move South (x, y) = (x, y - 1)
move East  (x, y) = (x + 1, y)
move West  (x, y) = (x - 1, y)

moves :: [Move] -> Pos -> Pos
moves []       p = p
moves (m : ms) p = moves ms (move m p)

-- Arguments in constructors

data Shape = Circle Float | Rectangle Float Float
  deriving (Show, Eq)

square :: Float -> Shape
square s = Rectangle s s

area :: Shape -> Float
area (Circle r     ) = pi * r ^ 2
area (Rectangle x y) = x * y

-- Tautologies.hs
-- AbstractMachine.hs

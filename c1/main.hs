double x = x + x

sumList a = sum [1 .. a]

qsort []       = []
qsort (x : xs) = qsort smaller ++ [x] ++ qsort larger
 where
  smaller = [ a | a <- xs, a <= x ]
  larger  = [ b | b <- xs, b > x ]

-- 1
  -- ... Just show the evaluation process

-- 2
  -- ...  Via evaluating it
  -- sum [x] ; x + sum [] ; x + 0

--3
myProduct []       = 1
myProduct (n : ns) = n * myProduct ns

--4
reverseQsort []       = []
reverseQsort (x : xs) = reverseQsort larger ++ [x] ++ reverseQsort smaller
 where
  smaller = [ a | a <- xs, a <= x ]
  larger  = [ b | b <- xs, b > x ]

--5 
otherQsort []       = []
otherQsort (x : xs) = otherQsort smaller ++ [x] ++ otherQsort larger
 where
  smaller = [ a | a <- xs, a < x ]
  larger  = [ b | b <- xs, b > x ]

 -- It only keeps one element for every repeated number
